﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace knockball{
public class GlassPieceShrink : MonoBehaviour {

	public float shrinkDuration = 2f;
	public float lifeline=3f;
	void Start()
	{
		Destroy(this.gameObject, lifeline);
	}
	void Update()
	{
		Vector3 shrink = Vector3.Lerp(transform.localScale, Vector3.zero, Time.deltaTime*shrinkDuration);
		transform.localScale = shrink;
	}
}
}