﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace knockball{
public class MovementScript : MonoBehaviour {

	public bool rotateX,rotateY,rotateZ, X, Y, Z;

	public Vector2 rotVal;
	public float moveXVal, moveYVal, moveZVal, moveTime;
	float rot, moveX,moveY;
	public bool dontDestroy;

	void Start()
	{
		rot = Random.Range(rotVal.x, rotVal.y);
		// moveX = Random.Range(moveXVal.x, moveXVal.y);
		// moveY = Random.Range(moveYVal.x, moveYVal.y);

		if (X)
		{			
			iTween.MoveBy(gameObject, iTween.Hash("x", moveXVal, "easeType", "Linear", "loopType", "pingPong", "time", moveTime));
		}
		else if (Y)
		{
			iTween.MoveBy(gameObject, iTween.Hash("y", moveYVal, "easeType", "Linear", "loopType", "pingPong", "time", moveTime));		
		}
		else if (Z)
		{
			iTween.MoveBy(gameObject, iTween.Hash("z", moveZVal, "easeType", "Linear", "loopType", "pingPong", "time", moveTime));		
		}
		
		
		
	}
	void Update()
	{
		if(rotateZ)
		{
			transform.Rotate(Vector3.forward * rot * Time.deltaTime);
		}
		else if(rotateX)
		{
			transform.Rotate(Vector3.right * rot * Time.deltaTime);
		}
		else if(rotateY)
		{
			transform.Rotate(Vector3.up * rot * Time.deltaTime);
		}

		
	}
}
}