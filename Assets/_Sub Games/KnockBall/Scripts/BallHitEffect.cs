﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace knockball{
	public class BallHitEffect : MonoBehaviour {

		public GameObject effect;
		public bool hit;
		AudioSource aS;
		public AudioClip[] hitSound;
		
		void Start()
		{
			aS = this.GetComponent<AudioSource>();
		}
		void OnCollisionEnter(Collision other)
		{
			if(other.collider.tag == "Obstacles" && !hit)
			{	
				hit = true;
				Instantiate(effect, transform.position, Quaternion.identity);
				
			}
			if(other.collider.tag == "Obstacles" )
			{
				aS.clip = hitSound[Random.Range(0, hitSound.Length)];
				aS.Play();
			}
			
		}
	}
}