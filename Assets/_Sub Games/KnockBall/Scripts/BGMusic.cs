﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace knockball{
	public class BGMusic : MonoBehaviour {

		void Start()
		{
			DontDestroyOnLoad(this.gameObject);
			GameObject[] bgMusic = GameObject.FindGameObjectsWithTag("BGMusic");
			if(bgMusic.Length >1)
			{
				Destroy(bgMusic[1]);
			}
		}
	}
}