﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace knockball{
public class Shrink : MonoBehaviour {

	public List<GameObject> objects;
	public float waitBeforeDestroy = 3f;
	public float durationToShrink = 2f;
	ButtonCanvas bc;

	void Start()
	{
		bc = GameObject.FindGameObjectWithTag("ButtonCanvas").GetComponent<ButtonCanvas>();
	}
	void Update()
	{
		if(objects.Count > 0)
		{
			for (int i = 0; i < objects.Count; i++)
			{
				if(objects[i] != null)
				{
					Vector3 desiredScale = Vector3.Lerp(objects[i].transform.localScale, Vector3.zero, Time.deltaTime * durationToShrink);
					objects[i].transform.localScale = desiredScale;
				}

			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Obstacles" || other.gameObject.tag == "Ball")
		{
			if(other.gameObject.tag == "Obstacles")
			{
				bc.AddScore();
			}
			other.gameObject.tag = "Untagged";
			objects.Add(other.gameObject);
			StartCoroutine(DestroyObjects(other.gameObject));
		}
	}
	void OnCollisionEnter(Collision other)
	{
		if(other.collider.tag == "Obstacles" || other.collider.tag == "Ball")
		{
			if(other.collider.tag == "Obstacles")
			{
				bc.AddScore();
			}
			other.collider.tag = "Untagged";
			objects.Add(other.gameObject);
			StartCoroutine(DestroyObjects(other.gameObject));
		}
	}

	IEnumerator DestroyObjects(GameObject other)
	{
		yield return new WaitForSeconds(waitBeforeDestroy);
		objects.Remove(other);
		Destroy(other);
	}
}
}