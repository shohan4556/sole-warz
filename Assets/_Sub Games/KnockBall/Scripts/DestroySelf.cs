﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace knockball{
public class DestroySelf : MonoBehaviour {

	public float lifeLine = 1f;


	void Start ()
	{
		Destroy(this.gameObject, lifeLine);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
}