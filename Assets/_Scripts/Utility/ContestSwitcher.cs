﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContestSwitcher : MonoBehaviour
{
    [SerializeField] private Button m_BattlesBtn;
    [SerializeField] private Button m_DrawsBtn;

    Color black = Color.black;
    Color white = Color.white;

    private void Awake() {
        m_BattlesBtn.onClick.AddListener(BattlesView);
        m_DrawsBtn.onClick.AddListener(DrawView);
    }

    private void BattlesView(){
        m_BattlesBtn.GetComponent<Image>().color = black;
        m_BattlesBtn.GetComponentInChildren<Text>().color = white;

        m_DrawsBtn.GetComponent<Image>().color = white;
        m_DrawsBtn.GetComponentInChildren<Text>().color = black;
    }

    private void DrawView(){
        m_BattlesBtn.GetComponent<Image>().color = white;
        m_BattlesBtn.GetComponentInChildren<Text>().color = black;

        m_DrawsBtn.GetComponent<Image>().color = black;
        m_DrawsBtn.GetComponentInChildren<Text>().color = white;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    
}
